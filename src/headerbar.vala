[GtkTemplate (ui = "/org/example/App/headerbar.ui")]
public class Focus.HeaderBar : Gtk.HeaderBar {
	public signal void on_focus ();

	[GtkCallback]
	private void focus_cb () {
		on_focus ();
	}
}
