[GtkTemplate (ui = "/org/example/App/window.ui")]
public class Focus.Window : Gtk.ApplicationWindow {
	[GtkChild]
	private HeaderBar headerbar;
	[GtkChild]
	private Gtk.Revealer headerbar_revealer;
	[GtkChild]
	private Gtk.Revealer unfocus_revealer;
	[GtkChild]
	private Gtk.TextView text;

	public Window (Gtk.Application app) {
		Object (application: app);
	}

	construct {
		var screen = Gdk.Screen.get_default ();
		var provider = new Gtk.CssProvider ();
		provider.load_from_resource ("/org/example/App/gtk.css");
		Gtk.StyleContext.add_provider_for_screen (screen, provider, 600);
	}

	[GtkCallback]
	private void focus_cb () {
		var height = headerbar.get_allocated_height ();
		headerbar.visible = false;
		headerbar_revealer.visible = true;
		headerbar_revealer.reveal_child = false;

		unfocus_revealer.reveal_child = true;
		get_style_context ().add_class ("focus");

		text.top_margin = 18 + height;
		text.queue_draw ();
	}

	[GtkCallback]
	private void unfocus_cb () {
		headerbar_revealer.reveal_child = true;
		unfocus_revealer.reveal_child = false;
	}

	[GtkCallback]
	private void child_revealed_cb () {
		if (!headerbar_revealer.reveal_child)
			return;

		headerbar.visible = true;
		headerbar_revealer.visible = false;
		get_style_context ().remove_class ("focus");

		text.top_margin = 18;
		text.queue_draw ();
	}
}
